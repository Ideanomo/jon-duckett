var $listItemHTML = $('li').html();
$('li').append('<i>' + $listItemHTML + '</i>');
/*
* The selector returns the four <li> elements
*.html() method returns the content of the first one.
* This is then appended to the end of the selection, in this case after each exisiting <li> element
*/