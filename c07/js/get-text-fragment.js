var $listText = $('ul').text();
$('ul').append('<p>' + $listText + '</p>');
/*
* The selector returns the <ul> element. The .text() method gets the text
* from all of the <ul> elemnts children. This is then appended to the end of the
* selection, after the existing  <ul> element. How is this different from using .html() method?
*/