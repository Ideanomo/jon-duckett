var $listHTML = $('ul').html();
$('ul').append($listHTML);
/*
* The selector returns the <ul> element. The .html() method gets all the HTML inside it (the four <li> elements)
* This is then appended to the end of the selection, in this case after the existing <li> elements
*/