function checkLength(e, minLength)              // Declare new function
{
    var el, elMsg;                              // Declare variables
    if (!e)
    {
      e = window.event;
    }
    el = e.target || e.srcElement;              // Get target of event
    elMsg = el.nextSibling;                     // Get its next sibling

    if (el.value.length < minLength)            // If length is too short set msg
    {
        elMsg.innerHTML = 'Username must be ' + minLength + ' character or more';
    }
    else                                        // otherwise
    {
        elMsg.innerHTML = '';                   // Clear message
    }
}

var elusername = document.getElementById('username');    // Get username input
if (elUsername.addEventListener)                        // If event listener supported
{
    elUsername.addEventListener('blur', function(e)    // On blur event
    {
        checkLength(e, 5);                          // Call checkUsername()
    }, false);                                       // Capture in bubble phase
}
else                                                // Otherwise
{
    elusername.attachEvent('onblur', function(e)    // IE fallback onblur
    {
        checkLength(e, 5);                        // Call checkUsername()
    });
}