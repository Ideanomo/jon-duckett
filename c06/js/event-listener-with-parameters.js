
var elUsername = document.getElementById('username');       // Get username input
var elMsg = document.getElementById('feedback');

function checkUsername(minLength)
{
    if(elUsername.value.length < minLength)
    {
        //Set error message
        elMsg.textContent = 'Username must be ' + minLength + ' characters or more';
    }
    else
    {
        elMsg.innerHTML = '';
    }
}

elUsername.addEventListener('blur', function()              // When it loses focus
{
   checkUsername(5);                                        // Pass arguments here
}, false);